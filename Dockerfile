FROM node:18-alpine

WORKDIR /app
COPY package.json .
COPY package-lock.json .

RUN npm install

ARG BC_NAME
COPY packages/$BC_NAME .
