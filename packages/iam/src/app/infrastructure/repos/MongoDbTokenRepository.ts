import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Token } from '../../domain/Token';
import { TokenRepository } from '../../domain/TokenRepository';
import { TokenSnapshot } from '../../domain/TokenSnapshot';

@Injectable()
export class MongoDbTokenRepository implements TokenRepository {
  public static readonly DB = Symbol.for('MongoDbTokenRepository.DB');
  private readonly tokenModel: mongoose.Model<TokenSnapshot>;
  private readonly collection: string = 'iam_tokens';

  constructor(
    @Inject(MongoDbTokenRepository.DB)
    private readonly connection: mongoose.Connection,
  ) {
    const tokenSchema = new mongoose.Schema({
      _id: { type: mongoose.Schema.Types.ObjectId, alias: 'id' },
      value: mongoose.Schema.Types.String,
    }, { versionKey: 'version' });

    this.tokenModel = this.connection.model('Token', tokenSchema, this.collection);
  }

  public async add(token: Token): Promise<void> {
    await this.tokenModel.create(token.toSnapshot());
  }

  public async find(tokenValue: string): Promise<Token | null> {
    const tokenSnapshot = await this.tokenModel.findOne({ value: tokenValue }).exec();

    if (!tokenSnapshot) throw new NotFoundException(`Token with value ${ tokenValue } not found`);

    return TokenSnapshot.toEntity(tokenSnapshot);
  }
}