import { Body, Controller, Post } from '@nestjs/common';
import { IAMApplicationService } from '../../application/IAMApplicationService';
import { TokenSnapshot } from '../../domain/TokenSnapshot';

@Controller('/iam')
export class IAMController {
  constructor(
    private readonly iamApplicationService: IAMApplicationService,
  ) {
  }

  @Post('/login')
  public async login(): Promise<TokenSnapshot> {
    return this.iamApplicationService.login();
  }

  @Post('/validate')
  public async validateAccess(
    @Body('token') token: string,
  ): Promise<boolean> {
    return this.iamApplicationService.validateToken(token);
  }
}