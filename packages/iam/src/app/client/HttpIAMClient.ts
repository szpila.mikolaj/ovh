import { Injectable, UnauthorizedException } from '@nestjs/common';
import axios from 'axios';
import { ConfigService } from '../ConfigService';
import { IAMClient } from '../domain/IAMClient';

@Injectable()
export class HttpIAMClient implements IAMClient {
  private readonly endpoint: string;

  constructor(
    private readonly configService: ConfigService,
  ) {
    this.endpoint = this.configService.get('IAM_ENDPOINT');
  }

  public async validateAccess(command: { tokenValue: string }): Promise<boolean> {
    const result = await axios.post(this.endpoint, { token: command.tokenValue });

    if (result.status !== 201) {
      throw new UnauthorizedException(`Token ${ command.tokenValue } is not valid`);
    }

    return true;
  }
}
