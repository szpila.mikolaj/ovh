import { Token, TokenID } from './Token';

export class TokenSnapshot {
  constructor(
    public readonly id: string,
    public readonly value: string,
  ) {
  }

  public static toEntity(snapshot: TokenSnapshot): Token {
    return new Token(
      new TokenID(snapshot.id),
      snapshot.value
    );
  }
}