import { Injectable } from '@nestjs/common';
import { Token } from './Token';

@Injectable()
export abstract class TokenRepository {
  public abstract add(token: Token): Promise<void>;

  public abstract find(tokenValue: string): Promise<Token | null>;
}