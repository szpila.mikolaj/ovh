import { Entity, Identifier } from '../../../../shared/src';
import { TokenSnapshot } from './TokenSnapshot';

export class TokenID extends Identifier {}

export class Token extends Entity<TokenID, TokenSnapshot> {
  constructor(
    id: TokenID,
    private value: string,
  ) {
    super(id);
  }

  public toSnapshot(): TokenSnapshot {
    return new TokenSnapshot(
      this.id.toString(),
      this.value
    );
  }
}
