import { Injectable } from '@nestjs/common';
import { Token, TokenID } from '../domain/Token';
import { TokenRepository } from '../domain/TokenRepository';
import { TokenSnapshot } from '../domain/TokenSnapshot';

@Injectable()
export class IAMApplicationService {
  constructor(
    private readonly tokenRepository: TokenRepository,
  ) {
  }

  public async login(): Promise<TokenSnapshot> {
    const tokenValue = new TokenID().toString();
    const token = new Token(new TokenID(), tokenValue);

    await this.tokenRepository.add(token);
    return token.toSnapshot();
  }

  public async validateToken(tokenValue: string): Promise<boolean> {
    return Boolean(await this.tokenRepository.find(tokenValue));
  }
}