import { Module } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { IAMApplicationService } from './application/IAMApplicationService';
import { HttpIAMClient } from './client/HttpIAMClient';
import { ConfigService } from './ConfigService';
import { IAMClient } from './domain/IAMClient';
import { TokenRepository } from './domain/TokenRepository';
import { IAMController } from './infrastructure/apis/IAMController';
import { MongoDbTokenRepository } from './infrastructure/repos/MongoDbTokenRepository';

@Module({
  imports: [],
  controllers: [IAMController],
  providers: [
    IAMApplicationService,
    ConfigService,
    {
      provide: 'MONGO_DB_CONNECTION',
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const dbConfig = configService.get<string>('MONGO_DB_CONNECTION');
        return mongoose.createConnection(dbConfig);
      },
    },
    { provide: MongoDbTokenRepository.DB, useExisting: 'MONGO_DB_CONNECTION' },
    { provide: TokenRepository, useClass: MongoDbTokenRepository },
    { provide: IAMClient, useClass: HttpIAMClient },
  ],
  exports: [IAMClient]
})
export class IAMModule {}
