import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class IAMClient {
  public abstract validateAccess(command: { tokenValue: string }): Promise<boolean>;
}
