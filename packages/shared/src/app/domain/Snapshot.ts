export abstract class Snapshot<E> {
  public abstract toEntity(): E;
}