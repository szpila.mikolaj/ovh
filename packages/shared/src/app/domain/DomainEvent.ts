type SimplePayloadValue = string | number | boolean | null;

export type SimplePayloadValueUnion = SimplePayloadValue | SimplePayload | SimplePayload[] | SimplePayloadValue[];

export type SimplePayload = {
  [key: string]: SimplePayloadValueUnion;
}

export interface IDomainEvent<P extends SimplePayload> {
  readonly $name: string;
  readonly $version: number;
  readonly occurredOn: Date;
  readonly payload: Readonly<P>;

  isInstanceOf<T>(Constructor: {new(...args: any[]): T}): this is T;
}

export abstract class DomainEvent<TPayload extends SimplePayload = SimplePayload>
  implements IDomainEvent<TPayload>
{
  /**
   * something like FQCN - fully qualified class name in JAVA
   * Unique event name <BoundedContext>/<EventClass>
   * Example: "@ppg/auth-and-identity/UserSignedIn"
   * Example: "@ppg/webpush/CampaignSent"
   * Example: "@ppg/accounting/PaymentAccepted"
   */
  abstract readonly $name: string;

  /**
   * Domain Event signature version
   * Always start with enumerating with 1
   */
  abstract readonly $version: number;

  /**
   * @return {Date} timestamp of event start
   */
  abstract readonly occurredOn: Date;

  public readonly payload: Readonly<TPayload>;

  constructor(payload : TPayload) {
    this.payload = payload;
  }

  /**
   * Checks if instance is and instance of given class constructor
   * @param Constructor
   */
  isInstanceOf<T>(Constructor: {new(...args: any[]): T}): this is T {
    return this instanceof Constructor;
  }

  public equals(other: any): boolean {
    if (!(other instanceof DomainEvent)) {
      return false;
    }

    return this.$name === other.$name &&
      this.$version === other.$version &&
      this.occurredOn.getTime() === other.occurredOn.getTime() &&
      comparePayloads(this.payload, other.payload);
  }

  /**
   * Compares events but ignores timestamp
   * @deprecated
   * @param other
   */
  public partialEqual(other: any): boolean {
    if (!(other instanceof DomainEvent)) {
      return false;
    }

    return this.$name === other.$name &&
      this.$version === other.$version &&
      comparePayloads(this.payload, other.payload);
  }

  static createEvent<T extends SimplePayload>(
    fullyQualifiedName: string,
    payload: T,
    version: number = 1,
  ): DomainEvent<T> {
    class Event extends DomainEvent<T> {
      readonly $name: string = fullyQualifiedName;
      readonly $version: number = version;
      readonly occurredOn: Date = new Date();
    }

    return new Event(payload);
  }
}

function comparePayloads<T extends Object>(first: T, second: T): boolean {
  for (const propertyName in first) {
    if (first.hasOwnProperty(propertyName)) {
      if (!second.hasOwnProperty(propertyName)) {
        return false;
      }

      const firstPropertyValue = first[propertyName];
      const secondPropertyValue = second[propertyName];

      // if (isEqualable(firstPropertyValue) && !firstPropertyValue.equals(secondPropertyValue)) {
      //   return false;
      // }

      if (firstPropertyValue !== secondPropertyValue) {
        return false;
      }
    }
  }

  return true;
}
