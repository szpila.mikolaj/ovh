import { Injectable, UnauthorizedException } from '@nestjs/common';
import axios from 'axios';
import { IAMClient } from '../domain/IAMClient';

@Injectable()
export class HttpIAMClient implements IAMClient {

  constructor(
    private readonly endpoint: string
  ) {
  }

  public async validateAccess(command: { tokenValue: string }): Promise<boolean> {
    const result = await axios.post(this.endpoint, { token: command.tokenValue });

    if (result.status !== 201) {
      throw new UnauthorizedException(`Token ${ command.tokenValue } is not valid`);
    }

    return true;
  }
}
