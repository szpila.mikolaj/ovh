import { Injectable } from '@nestjs/common';
import { IAMClient } from '../../../../shared/src/app/domain/IAMClient';
import { ResourceRepository } from '../domain/ResourceRepository';
import { ResourceSnapshot } from '../domain/ResourceSnapshot';

@Injectable()
export class ResourceApplicationService {
  constructor(
    private readonly resourceRepository: ResourceRepository,
    private readonly iamClient: IAMClient,
  ) {
  }

  public async getRestrictedResource(token: string): Promise<ResourceSnapshot> {
    const valid = await this.iamClient.validateAccess({ tokenValue: token });

    if (valid) {
      const resource = await this.resourceRepository.getRestrictedResource();
      return resource.toSnapshot();
    }
  }

  public async getNonRestrictedResource(): Promise<ResourceSnapshot> {
    const resource = await this.resourceRepository.getNonRestrictedResource();
    return resource.toSnapshot();
  }
}
