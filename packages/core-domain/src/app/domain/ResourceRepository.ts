import { Injectable } from '@nestjs/common';
import { Resource } from './Resource';

@Injectable()
export abstract class ResourceRepository {
  public abstract add(resource: Resource): Promise<void>;

  public abstract getRestrictedResource(): Promise<Resource>;

  public abstract getNonRestrictedResource(): Promise<Resource>;
}