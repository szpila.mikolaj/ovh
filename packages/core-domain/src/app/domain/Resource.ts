import { Entity, Identifier } from '../../../../shared/src';
import { ResourceSnapshot } from './ResourceSnapshot';

export class ResourceID extends Identifier {
}

export class Resource extends Entity<ResourceID, ResourceSnapshot> {
  constructor(
    id: ResourceID,
    private restrictedAccess: boolean,
  ) {
    super(id);
  }

  public toSnapshot(): ResourceSnapshot {
    return new ResourceSnapshot(
      this.id.toString(),
      this.restrictedAccess,
    );
  }
}
