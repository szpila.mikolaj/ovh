import { Resource, ResourceID } from './Resource';

export class ResourceSnapshot {
  constructor(
    public readonly id: string,
    public readonly restrictedAccess: boolean,
  ) {
  }

  public static toEntity(snapshot: ResourceSnapshot): Resource {
    return new Resource(
      new ResourceID(snapshot.id),
      snapshot.restrictedAccess
    )
  }
}