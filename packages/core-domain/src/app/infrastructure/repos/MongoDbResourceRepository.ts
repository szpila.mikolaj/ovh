import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import mongoose from 'mongoose';
import { Resource } from '../../domain/Resource';
import { ResourceRepository } from '../../domain/ResourceRepository';
import { ResourceSnapshot } from '../../domain/ResourceSnapshot';

@Injectable()
export class MongoDbResourceRepository implements ResourceRepository {
  public static readonly DB = Symbol.for('MongoDbResourceRepository.DB');
  private readonly resourceModel: mongoose.Model<ResourceSnapshot>;
  private readonly collection: string = 'core_domain_resources';

  constructor(
    @Inject(MongoDbResourceRepository.DB)
    private readonly connection: mongoose.Connection,
  ) {
    const resourceSchema = new mongoose.Schema({
      _id: { type: mongoose.Schema.Types.ObjectId, alias: 'id' },
      restrictedAccess: mongoose.Schema.Types.Boolean,
    }, { versionKey: 'version' });

    this.resourceModel = this.connection.model('Resource', resourceSchema, this.collection);
  }

  public async add(resource: Resource): Promise<void> {
    await this.resourceModel.create(resource.toSnapshot());
  }

  public async getNonRestrictedResource(): Promise<Resource> {
    const resource = await this.resourceModel.findOne({ restrictedAccess: false });

    if (!resource) throw new NotFoundException(`Resource not found`);

    return ResourceSnapshot.toEntity(resource);
  }

  public async getRestrictedResource(): Promise<Resource> {
    const resource = await this.resourceModel.findOne({ restrictedAccess: true });

    if (!resource) throw new NotFoundException(`Resource not found`);

    return ResourceSnapshot.toEntity(resource);
  }
}