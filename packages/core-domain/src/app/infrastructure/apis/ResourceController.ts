import { Controller, Get, Headers } from '@nestjs/common';
import { ResourceApplicationService } from '../../application/ResourceApplicationService';
import { ResourceSnapshot } from '../../domain/ResourceSnapshot';

@Controller('/resource')
export class ResourceController {
  constructor(
    private readonly resourceApplicationService: ResourceApplicationService,
  ) {
  }

  @Get('/non-restricted')
  public async getNonRestrictedResource(): Promise<ResourceSnapshot> {
    return this.resourceApplicationService.getNonRestrictedResource();
  }

  @Get('/restricted')
  public async getRestrictedResource(
    @Headers('x-token') token: string,
  ): Promise<ResourceSnapshot> {
    return this.resourceApplicationService.getRestrictedResource(token);
  }
}