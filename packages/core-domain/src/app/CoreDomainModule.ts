import { Module } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { IAMClient } from '../../../shared/src/app/domain/IAMClient';
import { HttpIAMClient } from '../../../shared/src/app/infrastructure/HttpIAMClient';
import { ResourceApplicationService } from './application/ResourceApplicationService';
import { ConfigService } from './ConfigService';
import { ResourceRepository } from './domain/ResourceRepository';
import { ResourceController } from './infrastructure/apis/ResourceController';
import { MongoDbResourceRepository } from './infrastructure/repos/MongoDbResourceRepository';

@Module({
  imports: [],
  controllers: [ResourceController],
  providers: [
    ResourceApplicationService,
    ConfigService,
    {
      provide: 'MONGO_DB_CONNECTION',
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const dbConfig = configService.get<string>('MONGO_DB_CONNECTION');
        return mongoose.createConnection(dbConfig);
      }
    },
    { provide: MongoDbResourceRepository.DB, useExisting: 'MONGO_DB_CONNECTION' },
    { provide: ResourceRepository, useClass: MongoDbResourceRepository },
    {
      provide: IAMClient,
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return new HttpIAMClient(config.get<string>('IAM_ENDPOINT'));
      }
    }
  ]
})
export class CoreDomainModule {
}
