/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { CoreDomainModule } from './app/CoreDomainModule';

async function bootstrap() {
  const app = await NestFactory.create(CoreDomainModule);
  // const globalPrefix = 'api';
  // app.setGlobalPrefix(globalPrefix);
  app.enableCors();
  const port = process.env.PORT || 3333;
  await app.listen(port);
  Logger.log(
    // `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
    `🚀 Core-domain is running on: http://localhost:${port}`
  );
}

bootstrap();
